import {withApollo} from "@apollo/client/react/hoc";
import {useMutation, useQuery} from "@apollo/client";
import {DOWNLOAD_FILE_BY_KEY, LIST_FILES_QUERY, UPLOAD_FILES_BASE64_MUTATION} from "../../Queries";
import {useState} from "react";

function File() {

    let files = [];
    let [upload] = useMutation(UPLOAD_FILES_BASE64_MUTATION);
    let [downloadBase64] = useMutation(DOWNLOAD_FILE_BY_KEY);
    let [loading, setLoading] = useState(false);

    const getAllFiles = useQuery(LIST_FILES_QUERY);

    let downloadFile = (event) => {
        let filename = event.target['outerText'];
        downloadBase64({variables: {key: filename}})
            .then(value => {
                let fileB64 = value.data['downloadFile']['base64'];
                let linkDownload = document.getElementById("id" + filename);
                linkDownload.href = `data:application/json;base64,${fileB64}`;
                linkDownload.download = filename;
                linkDownload.innerText = filename + " - Download"
            }).catch(reason => reason);
    }

    let convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file)
            fileReader.onload = () => {
                resolve(fileReader.result);
            }
            fileReader.onerror = (error) => {
                reject(error);
            }
        })
    }

    let uploadFiles = async function () {
        setLoading(true);
        let forms = []
        for (let f of files) {
            const base64 = await convertBase64(f);
            forms.push({
                filename: f.name.toString(),
                filetype: f.type.toString(),
                base64: base64.toString().substring(base64.toString().indexOf(",") + 1)
            })
        }
        upload({variables: {"files": forms}})
            .then(value => {
                alert(value.data.uploadBase64);
                window.location.reload();
            })
            .catch(reason => alert(reason))
    }

    if (getAllFiles.loading) return <div>Loading...</div>;
    if (loading) return <div>Uploading...</div>;
    return (
        <div className="Files">
            <h1>Upload Files</h1>
            <label htmlFor="files">Select files:</label>
            <input type="file" multiple onChange={event => files = event.target.files}/>
            <div className="col-4">
                <button
                    className="btn-upload"
                    onClick={uploadFiles}
                >Upload
                </button>
            </div>

            <div className='processedFiles'>
                <h1>All Files Processed!</h1>
                <table>
                    <tbody>
                        {getAllFiles.data.listFiles.map((a) =>
                            <tr key={a}>
                                <td key={'td' + a}>
                                    <a id={"id" + a} href="#" onClick={event => downloadFile(event)}>{a}</a>
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>

            </div>

        </div>);
}

export default withApollo(File);