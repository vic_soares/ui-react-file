import gql from "graphql-tag";

export const UPLOAD_FILES_MUTATION = gql`
    mutation upload($file: [Upload!]!) {
        upload(file: $file), {filename}
    }
`;

export const UPLOAD_FILES_BASE64_MUTATION = gql`
    mutation uploadBase64($files: [fileInput]!) {
        uploadBase64(files: $files)
    }
`;

export const DOWNLOAD_FILE_BY_KEY = gql`
    mutation downloadFile($key: String!) {
        downloadFile(key: $key), {
            base64, filename
        }
    }
`;

export const LIST_FILES_QUERY = gql`
    query { listFiles }
`;
